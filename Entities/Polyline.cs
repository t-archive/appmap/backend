﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace HelloWorldApi.Entities
{
    [Table("polyline_list", Schema = "data")]
    public class Polyline
    {
        [Key] [Column("id")] 
        public int Id { get; set; }

        [Column("lat")] 
        public float Latitude { get; set; }

        [Column("lng")] 
        public float Longitude { get; set; }

        [Column("time")] 
        public int Time { get; set; }

        [Column("speed")] 
        public float Speed { get; set; }

        [Column("bearing")] 
        public float Bearing { get; set; }

        [Column("accuracy")] 
        public float Accuracy { get; set; }

        [Column("altitude")] 
        public float Altitude { get; set; }

        [Column("record")] 
        public bool Record { get; set; }


        private static void OnModelCreating(ModelBuilder builder)
        {
#if DEBUG
            //builder.Entity<MyMarker>().HasData(
            //    new MyMarker()
            //    {
            //        Id = 1,
            //        Latitude = 0,
            //        Longitude = 0,
            //        Description = null,
            //        SmallDescription = null
            //    },
            //    new MyMarker()
            //    {
            //        Id = 2,
            //        Latitude = 0,
            //        Longitude = 0,
            //        Description = null,
            //        SmallDescription = null
            //    },
            //    new MyMarker()
            //    {
            //        Id = 3,
            //        Latitude = 0,
            //        Longitude = 0,
            //        Description = null,
            //        SmallDescription = null
            //    }
            //);
#endif
        }
    }
}