﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace HelloWorldApi.Entities
{
    [Table("my_marker_list", Schema = "data")]
    public class MyMarker
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        [Column("lat")]
        public float Latitude { get; set; } 
        
        [Column("lng")]
        public float Longitude { get; set; }
        
        [Column("description")]
        public string Description { get; set; }
        
        [Column("small_description")]
        public string SmallDescription { get; set; }
        
        
        
        private static void OnModelCreating(ModelBuilder builder)
        {
#if DEBUG
            builder.Entity<MyMarker>().HasData(
                new MyMarker()
                {
                    Id = 1,
                    Latitude = 0,
                    Longitude = 0,
                    Description = null,
                    SmallDescription = null
                },
                new MyMarker()
                {
                    Id = 2,
                    Latitude = 0,
                    Longitude = 0,
                    Description = null,
                    SmallDescription = null
                },
                new MyMarker()
                {
                    Id = 3,
                    Latitude = 0,
                    Longitude = 0,
                    Description = null,
                    SmallDescription = null
                }
            );
#endif
        }
        
    }
}