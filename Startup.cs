using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using HelloWorldApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace HelloWorldApi
{
    public class Startup
    {
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PolylineDb>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Default"));
            });
#if DEBUG
            // Datenbank beim Start der API löschen und neu anlegen
            var dbContext2 = services.BuildServiceProvider().GetService<PolylineDb>();
            dbContext2.Database.EnsureDeleted();
            dbContext2.Database.EnsureCreated();
#endif

            services.AddDbContext<MyMarkerDb>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Default"));
            });
#if DEBUG
            // Datenbank beim Start der API löschen und neu anlegen
            var dbContext = services.BuildServiceProvider().GetService<MyMarkerDb>();
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();

#endif


            services.AddControllers();
            services.AddScoped<MyMarkerService>();
            services.AddScoped<PolylineService>();
            services.AddScoped<MyMarkerDb>();
            services.AddScoped<PolylineDb>();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseCors(builder => builder.WithOrigins("*").AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}