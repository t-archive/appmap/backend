﻿using HelloWorldApi.Entities;
using HelloWorldApi.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace HelloWorldApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MyMarkerController : ControllerBase
    {
        private readonly MyMarkerDb _dbContext;
        private readonly MyMarkerService _myMarkerService;


        public MyMarkerController(MyMarkerDb myMarkerDb, MyMarkerService myMarkerService)
        {
            _dbContext = myMarkerDb;
            _myMarkerService = myMarkerService;
        }

        [HttpGet("test")]
        public IActionResult Test()
        {
            return Ok();
        }


        [HttpPost("create")]
        public async Task<ActionResult<MyMarker>> Create(MyMarker myMarker)
        {
            await _dbContext.MyMarkers.AddAsync(myMarker);
            await _dbContext.SaveChangesAsync();
            return CreatedAtAction(nameof(GetAll), new {id = myMarker.Id}, myMarker);
        }

        [HttpPut("edit")]
        public async Task<ActionResult<MyMarker>> Edit(MyMarker myMarker)
        {
            _dbContext.MyMarkers.Update(myMarker);
            await _dbContext.SaveChangesAsync();
            return CreatedAtAction(nameof(GetAll), new {id = myMarker.Id}, myMarker);
        }

        // [HttpDelete("delete")]

        [HttpPost("delete")]
        public async Task<ActionResult<MyMarker>> Delete(MyMarker myMarker)
        {
            _dbContext.MyMarkers.Remove(myMarker);
            await _dbContext.SaveChangesAsync();
            return CreatedAtAction(nameof(GetAll), new {id = myMarker.Id}, myMarker);
        }

        [HttpGet("get")]
        public async Task<IActionResult> GetAll()
        {
            var myMarkers = await _dbContext.MyMarkers.ToListAsync();
            return Ok(myMarkers);
        }


        [HttpGet("get/id/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var myMarkers = await _dbContext.MyMarkers.ToListAsync();
            myMarkers = myMarkers.FindAll((x) => x.Id == id);
            return Ok(myMarkers);
        }


        [HttpGet("get-test")]
        public IActionResult GetAllTest()
        {
            var markers = _myMarkerService.GetAll();
            return Ok(markers);
        }
    }
}