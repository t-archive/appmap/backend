﻿using HelloWorldApi.Entities;
using HelloWorldApi.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace HelloWorldApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PolylineController : ControllerBase
    {
        private readonly PolylineDb _dbContext;
        private readonly PolylineService _polylineService;


        public PolylineController(PolylineDb polylineDb, PolylineService polylineService)
        {
            _dbContext = polylineDb;
            _polylineService = polylineService;
        }

        [HttpGet("test")]
        public IActionResult Test()
        {
            return Ok();
        }


        [HttpPost("create")]
        public async Task<ActionResult<Polyline>> Create(Polyline polyline)
        {
            await _dbContext.Polylines.AddAsync(polyline);
            await _dbContext.SaveChangesAsync();
            return CreatedAtAction(nameof(GetAll), new {id = polyline.Id}, polyline);
        }

        [HttpPut("edit")]
        public async Task<ActionResult<Polyline>> Edit(Polyline polyline)
        {
            _dbContext.Polylines.Update(polyline);
            await _dbContext.SaveChangesAsync();
            return CreatedAtAction(nameof(GetAll), new {id = polyline.Id}, polyline);
        }

        // [HttpDelete("delete")]

        [HttpPost("delete")]
        public async Task<ActionResult<Polyline>> Delete(Polyline polyline)
        {
            _dbContext.Polylines.Remove(polyline);
            await _dbContext.SaveChangesAsync();
            return CreatedAtAction(nameof(GetAll), new {id = polyline.Id}, polyline);
        }

        [HttpGet("get")]
        public async Task<IActionResult> GetAll()
        {
            var polyline = await _dbContext.Polylines.ToListAsync();
            return Ok(polyline);
        }


        [HttpGet("get/id/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var polylineList = await _dbContext.Polylines.ToListAsync();
            polylineList = polylineList.FindAll((x) => x.Id == id);
            return Ok(polylineList);
        }


        [HttpGet("get-test")]
        public IActionResult GetAllTest()
        {
            var markers = _polylineService.GetAll();
            return Ok(markers);
        }
    }
}