﻿using System;
using System.Collections.Generic;
using System.Linq;
using HelloWorldApi.Entities;

namespace HelloWorldApi.Services
{
    public interface IPolylineService : IDisposable
    {
        IEnumerable<Polyline> GetAll();
        Polyline GetById(int id);
    }

    public class PolylineService : IPolylineService
    {
        private List<Polyline> _polylinesList = new List<Polyline>
        {
            
        };

        public PolylineService()
        {
        }

        public IEnumerable<Polyline> GetAll()
        {
            return _polylinesList;
        }

        public Polyline GetById(int id)
        {
            return _polylinesList.FirstOrDefault(x => x.Id == id);
        }

        public void Dispose()
        {
        }
    }
}