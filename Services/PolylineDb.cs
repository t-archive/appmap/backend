﻿using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using HelloWorldApi.Entities;

namespace HelloWorldApi.Services
{
    public class PolylineDb : DbContext
    {
        public virtual DbSet<Polyline> Polylines { get; set; }

        public PolylineDb(DbContextOptions<PolylineDb> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Reflection...
            foreach (var t in GetType().GetProperties().Where(x =>
                    x.PropertyType.IsGenericType && x.PropertyType.Name.StartsWith("DbSet"))
                .Select(x => x.PropertyType.GetGenericArguments().Single()))
            {
                var mi = t.GetMethod(nameof(OnModelCreating), BindingFlags.Static | BindingFlags.NonPublic);
                if (mi != null)
                {
                    var paramInfo = mi.GetParameters();
                    if (paramInfo.Length == 1 && paramInfo[0].ParameterType == typeof(ModelBuilder))
                    {
                        mi.Invoke(null, new[] {builder});
                    }
                }
            }
        }
    }
}