﻿using System;
using System.Collections.Generic;
using System.Linq;
using HelloWorldApi.Entities;

namespace HelloWorldApi.Services
{
    public interface IMyMarkerService : IDisposable
    {
        IEnumerable<MyMarker> GetAll();
        MyMarker GetById(int id);
    }

    public class MyMarkerService : IMyMarkerService
    {
        private List<MyMarker> _markerList = new List<MyMarker>
        {
            new MyMarker() {Id = 1, Description = "Test"}
        };

        public MyMarkerService()
        {
        }

        public IEnumerable<MyMarker> GetAll()
        {
            return _markerList;
        }

        public MyMarker GetById(int id)
        {
            return _markerList.FirstOrDefault(x => x.Id == id);
        }

        public void Dispose()
        {
        }
    }
}